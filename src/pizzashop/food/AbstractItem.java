package pizzashop.food;

public abstract class AbstractItem implements OrderItem{

	public double getPrice(int size,double [] prices){
		double price = 0;
		if ( size >= 0 && size < prices.length ) price = prices[size];
		return price;
	}

	
}
